package co.pokeapi;

import io.qameta.allure.Description;
import co.pokeapi.model.Pokemon;
import org.junit.Test;
import co.pokeapi.steps.Steps;

import java.util.List;

public class ApiTest extends BaseTest {

    String pokemonRattata = "rattata";
    String pokemonPidgeotto = "pidgeotto";
    String pokemonGetra = "getra";
    String ability = "run-away";

    @Test
    @Description("Сравнение веса у двух покемонов")
    public void comparePokemonsWeight() {
        Pokemon pokemon1 = Steps.getPokemonByName(requestSpecification, pokemonRattata);
        Pokemon pokemon2 = Steps.getPokemonByName(requestSpecification, pokemonPidgeotto);
        Steps.checkHeavier(pokemon1, pokemon2);
    }

    @Test
    @Description("Создание покемона, негативный тест")
    public void negativeCreatePokemon() {
        Steps.checkStatusCode(404, Steps.getStatusCode(requestSpecification, pokemonGetra));
    }

    @Test
    @Description("Проверка на наличие и отсутствие умения run-away")
    public void checkPokemonsAbilities() {
        Steps.checkAbilityExist(requestSpecification, ability, pokemonRattata);
        Steps.checkAbilityNotExist(requestSpecification, ability, pokemonPidgeotto);
    }

    @Test
    @Description("Проверка ограничения списка покемонов и то, что у каждого есть имя")
    public void checkLimitedList() {
        int limit = 10;
        List<Pokemon> limitedList = Steps.getLimitedList(requestSpecification, limit);
        Steps.checkLimitedListSize(limitedList, limit);
        Steps.checkNamesLimitedList(limitedList);
    }
}
