package co.pokeapi;

import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.given;

public abstract class BaseTest {

    protected final RequestSpecification requestSpecification = given()
            .filter(new AllureRestAssured())
            .filter(new ResponseLoggingFilter())
            .baseUri("https://pokeapi.co/api/v2/pokemon")
            .contentType(ContentType.JSON);
}
