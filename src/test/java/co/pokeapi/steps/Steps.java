package co.pokeapi.steps;

import co.pokeapi.model.Pokemon;
import io.qameta.allure.Step;
import io.restassured.specification.RequestSpecification;
import co.pokeapi.model.Pokemon;
import org.junit.Assert;

import java.util.List;

public class Steps {

    @Step("Получить покемона по имени  {name}")
    public static Pokemon getPokemonByName(RequestSpecification requestSpecification, String name) {
        return requestSpecification
                .pathParam("name", name)
                .when()
                .get("/{name}")
                .then()
                .statusCode(200)
                .extract()
                .as(Pokemon.class);
    }

    @Step("Сделать запрос на поиск покемона по имени {name}")
    public static int getStatusCode(RequestSpecification requestSpecification, String name) {
        return requestSpecification
                .pathParam("name", name)
                .when()
                .get("/{name}")
                .then()
                .extract()
                .statusCode();
    }

    @Step("Проверить, что status code: {actual}")
    public static void checkStatusCode(int expected, int actual) {
        Assert.assertEquals(expected, actual);
    }

    @Step("Сравнить weight у двух покемонов (значение у первого меньше чем у второго)")
    public static void checkHeavier(Pokemon pokemon1, Pokemon pokemon2) {
        Assert.assertTrue(pokemon2.getWeight() > pokemon1.getWeight());
    }

    @Step("Проверить наличие умения {ability} у покемона {name}")
    public static void checkAbilityExist(RequestSpecification requestSpecification, String ability, String name) {
        Assert.assertTrue(getAbilitiesListByName(requestSpecification, name).contains(ability));
    }

    @Step("Проверить отсутствие умения {ability} у покемона {name}")
    public static void checkAbilityNotExist(RequestSpecification requestSpecification, String ability, String name) {
        Assert.assertFalse(getAbilitiesListByName(requestSpecification, name).contains(ability));
    }

    // Получить все ability у покемона по имени
    private static List<String> getAbilitiesListByName(RequestSpecification requestSpecification, String name) {
        return requestSpecification.pathParam("name", name)
                .when()
                .get("/{name}")
                .then()
                .statusCode(200)
                .extract()
                .jsonPath()
                .getList("abilities.ability.name");
    }

    @Step("Получить ограниченный список, лимит {limit}")
    public static List<Pokemon> getLimitedList(RequestSpecification requestSpecification, int limit) {
        List<Pokemon> pokemons = requestSpecification.queryParam("limit", limit)
                .when()
                .get()
                .then()
                .statusCode(200)
                .extract()
                .jsonPath()
                .getList("results", Pokemon.class);
        return pokemons;
    }

    @Step("Проверить, что размер ограниченного списка равен лимиту {limit}")
    public static void checkLimitedListSize(List<Pokemon> limitedList, int limit) {
        Assert.assertEquals(limit, limitedList.size());
    }

    @Step("Проверить, что у каждого покемона в ограниченном списке есть имя")
    public static void checkNamesLimitedList(List<Pokemon> limitedList) {
        for (Pokemon pokemon : limitedList) {
            Assert.assertNotNull(pokemon.getName());
        }
    }
}
