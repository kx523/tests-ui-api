package com.globalsqa;

import com.globalsqa.pages.AddCustomerPage;
import com.globalsqa.pages.BasePage;
import com.globalsqa.pages.CustomersPage;
import com.globalsqa.pages.MainPage;
import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.Description;
import io.qameta.allure.Epic;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

@Epic("Автотесты страниц")
@RunWith(DataProviderRunner.class)
public class TestCustomers {

    WebDriver driver;
    MainPage mainPage;
    AddCustomerPage addCustomerPage;
    CustomersPage customersPage;
    WebDriverWait wait;

    @BeforeClass
    public static void setupClass() {
        WebDriverManager.chromedriver().setup();
    }

    @Before
    public void setup() {
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        BasePage.setDriver(driver);
        mainPage = new MainPage();
        addCustomerPage = new AddCustomerPage();
        customersPage = new CustomersPage();
        wait = new WebDriverWait(driver, 10);
        driver.get("https://www.globalsqa.com/angularJs-protractor/BankingProject/#/manager");
        wait.until(ExpectedConditions.urlContains("BankingProject/#/manager"));
    }

    @DataProvider
    public static Object[][] oneMatch() {
        return new Object[][]{
                {"Albus", "Albus", "Dumbledore", "E55656"},
                {"dumb", "Albus", "Dumbledore", "E55656"}
        };
    }

    @Test
    @Description("Кейс 4, 5")
    @UseDataProvider("oneMatch")
    public void searchCustomerOneMatch(String searchName, String firstName, String lastName, String postCode) {
        mainPage.clickCustomers();
        wait.until(ExpectedConditions.urlContains("/manager/list"));
        customersPage.inputSearchCustomer(searchName)
                .checkCustomerIsFound(searchName, 1)
                .checkValues(firstName, lastName, postCode);
    }

    @DataProvider
    public static Object[][] multiMatch() {
        return new Object[][]{
                {"er", 2},
                {"e55", 2}
        };
    }

    @Test
    @Description("Кейс 7, 8")
    @UseDataProvider("multiMatch")
    public void searchCustomerMultiMath(String searchName, int match) {
        mainPage.clickCustomers();
        wait.until(ExpectedConditions.urlContains("/manager/list"));
        customersPage.inputSearchCustomer(searchName)
                .checkCustomerIsFound(searchName, match);
    }

    @Test
    @Description("Кейс 1. Создание клиента")
    public void addToCustomer() {
        String firstName = "FirstNameUser";
        String lastName = "LastNameUser";
        String codePost = "PostCodeUser";
        mainPage.clickAddCustomer();
        wait.until(ExpectedConditions.urlContains("/manager/addCust"));
        addCustomerPage.signUp(firstName, lastName, codePost)
                .clickAddCustomer()
                .checkAlertText("Customer added successfully with customer id :")
                .closeAlert();
        mainPage.clickCustomers();
        wait.until(ExpectedConditions.urlContains("/manager/list"));
        customersPage.inputSearchCustomer(firstName)
                .checkCustomerIsFound(firstName, 1)
                .checkValues(firstName, lastName, codePost);
    }

    @Test
    @Description("Кейс 2. Создание клиента, негативный тест")
    public void addToCustomerNegative() {
        mainPage.clickAddCustomer();
        wait.until(ExpectedConditions.urlContains("/manager/addCust"));
        addCustomerPage.signUp("Harry", "Potter", "E725JB")
                .clickAddCustomer()
                .checkAlertText("Please check the details. Customer may be duplicate.")
                .closeAlert();
    }

    @Test
    @Description("Кейс 3. Сортировка клиентов по имени")
    public void sortOfFirstName() {
        mainPage.clickCustomers();
        wait.until(ExpectedConditions.urlContains("/manager/list"));
        customersPage.clickFirstNameHeader()
                .checkSorting("desc")
                .clickFirstNameHeader()
                .checkSorting("asc");
    }

    @Test
    @Description("Кейс 6. Негативный тест, проверка, что клиент не найден")
    public void searchCustomerNegativeTest() {
        mainPage.clickCustomers();
        wait.until(ExpectedConditions.urlContains("/manager/list"));
        customersPage.inputSearchCustomer("Ivan")
                .checkCustomerIsNotFound("Ivan");
    }

    @After
    public void closeBrowser() {
        driver.quit();
    }
}
