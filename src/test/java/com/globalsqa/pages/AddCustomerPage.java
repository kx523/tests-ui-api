package com.globalsqa.pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static org.assertj.core.api.Assertions.assertThat;

public class AddCustomerPage extends BasePage {

    @FindBy(xpath = "//input[@ng-model='fName']")
    private WebElement firstName;

    @FindBy(xpath = "//input[@ng-model='lName']")
    private WebElement lastName;

    @FindBy(xpath = "//input[@ng-model='postCd']")
    private WebElement postCode;

    @FindBy(xpath = "//button[@class='btn btn-default']")
    private WebElement addCustomerButton;

    public AddCustomerPage() {
        PageFactory.initElements(driver, this);
    }

    @Step("Заполнить поля данными: {firstName}, {lastName}, {postCode}")
    public AddCustomerPage signUp(String firstName, String lastName, String postCode) {
        this.firstName.sendKeys(firstName);
        this.lastName.sendKeys(lastName);
        this.postCode.sendKeys(postCode);
        return this;
    }

    @Step("Нажать на кнопку Add Customer")
    public AddCustomerPage clickAddCustomer() {
        addCustomerButton.click();
        return this;
    }

    @Step("Закрыть модальное окно")
    public AddCustomerPage closeAlert() {
        driver.switchTo().alert().accept();
        return this;
    }

    @Step("Проверить текст в модальном окне")
    public AddCustomerPage checkAlertText(String text) {
        assertThat(driver.switchTo().alert().getText()).contains(text);
        return this;
    }
}
