package com.globalsqa.pages;

import io.qameta.allure.Step;
import org.assertj.core.api.SoftAssertions;
import org.junit.Assert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CustomersPage extends BasePage {

    @FindBy(xpath = "//a[contains(@ng-click,'fName')]")
    private WebElement firstNameHeader;

    @FindBy(xpath = "//input[@ng-model='searchCustomer']")
    private WebElement searchCustomer;

    @FindBy(xpath = "//tr[@class='ng-scope']/td[@class='ng-binding'][1]")
    private WebElement firstNameValue;

    @FindBy(xpath = "//tr[@class='ng-scope']/td[@class='ng-binding'][2]")
    private WebElement lastNameValue;

    @FindBy(xpath = "//tr[@class='ng-scope']/td[@class='ng-binding'][3]")
    private WebElement postCodeValue;

    // Значения в таблице в первой колонке (имена)
    @FindBy(xpath = "//tr[@class='ng-scope'][*]/td[@class='ng-binding'][1]")
    private List<WebElement> listFirstNames;

    // Значения в таблице
    @FindBy(xpath = "//tr[@class='ng-scope'][*]/td[@class='ng-binding']")
    private List<WebElement> listAllValues;

    // Строки в таблице
    @FindBy(xpath = "//tr[@class='ng-scope']")
    private List<WebElement> rows;

    public CustomersPage() {
        PageFactory.initElements(driver, this);
    }

    @Step("Нажать на заголовок First Name в таблице")
    public CustomersPage clickFirstNameHeader() {
        firstNameHeader.click();
        return this;
    }

    @Step("Ввести данные в поле поиска: {name}")
    public CustomersPage inputSearchCustomer(String name) {
        this.searchCustomer.sendKeys(name);
        return this;
    }

    @Step("Проверить, что имена отсортированы {sorting}")
    public CustomersPage checkSorting(String sorting) {
        ArrayList<String> sortedList = getListNames();
        switch (sorting) {
            case "desc":
                Collections.sort(sortedList, Collections.reverseOrder());
                break;
            case "asc":
                Collections.sort(sortedList);
                break;
            default:
                System.out.println("Параметр неверен: " + sorting);
        }
        Assert.assertEquals(sortedList, getListNames());
        return this;
    }

    @Step("Проверить, что поля заполнены данными: {firstName}, {lastName}, {postCode}")
    public CustomersPage checkValues(String firstName, String lastName, String postCode) {
        SoftAssertions softAssertions = new SoftAssertions();
        softAssertions.assertThat(this.firstNameValue.getText()).isEqualTo(firstName);
        softAssertions.assertThat(this.lastNameValue.getText()).isEqualTo(lastName);
        softAssertions.assertThat(this.postCodeValue.getText()).isEqualTo(postCode);
        softAssertions.assertAll();
        return this;
    }

    @Step("Проверить, что клиент(ы) по значению {value} найден(ы), и в таблице {number} строк(а)")
    public CustomersPage checkCustomerIsFound(String value, int number) {
        Assert.assertNotNull("Клиент не найден", searchCustomerTableByValue(value));
        Assert.assertEquals(number, getNumberRows());
        return this;
    }

    // Перегруженный метод (если не нужно проверять количество строк в таблице)
    @Step("Проверить, что клиент {value} найден")
    public CustomersPage checkCustomerIsFound(String value) {
        Assert.assertNotNull("Клиент не найден", searchCustomerTableByValue(value));
        Assert.assertNotEquals(0, getNumberRows());
        return this;
    }

    @Step("Проверить, что клиент {value} не найден")
    public CustomersPage checkCustomerIsNotFound(String value) {
        Assert.assertNull("Клиент найден", searchCustomerTableByValue(value));
        Assert.assertEquals(0, getNumberRows());
        return this;
    }

    @Step("Очистить строку поиска")
    public CustomersPage clearSearchField() {
        searchCustomer.sendKeys(Keys.CONTROL + "A");
        searchCustomer.sendKeys(Keys.BACK_SPACE);
        return this;
    }

    // Поиск клиента в таблице по введенному значению (независимо от регистра)
    private WebElement searchCustomerTableByValue(String value) {
        for (WebElement cell : listAllValues) {
            if (cell.getText().toLowerCase().contains(value.toLowerCase())) {
                return cell;
            }
        }
        return null;
    }

    // Получение списка имён клиентов из таблицы
    private ArrayList<String> getListNames() {
        ArrayList<String> listOfNames = new ArrayList<>();
        for (WebElement list : listFirstNames) {
            listOfNames.add(list.getText());
        }
        return listOfNames;
    }

    // Получение количества строк (клиентов) в таблице
    private int getNumberRows() {
        return rows.size();
    }
}
