package com.globalsqa.pages;

import io.qameta.allure.Step;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MainPage extends BasePage {

    @FindBy(xpath = "//button[@ng-class='btnClass1']")
    private WebElement addCustomerButton;

    @FindBy(xpath = "//button[@ng-class='btnClass3']")
    private WebElement customersButton;

    public MainPage() {
        PageFactory.initElements(driver, this);
    }

    @Step("Нажать на кнопку Add Customers")
    public MainPage clickAddCustomer() {
        addCustomerButton.click();
        return this;
    }

    @Step("Нажать на кнопку Customers")
    public MainPage clickCustomers() {
        customersButton.click();
        return this;
    }
}
